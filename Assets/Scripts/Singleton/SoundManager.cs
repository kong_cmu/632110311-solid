using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SoundManager : Singleton<ScoreManager>
{
    [SerializeField] private AudioSource sfxSource;
    
    protected override void Awake()
    {
        sfxSource = gameObject.AddComponent<AudioSource>();
    }

    public void PlaySfx(AudioClip _clip)
    {
        sfxSource.PlayOneShot(_clip);
    }
}
