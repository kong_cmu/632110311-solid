using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    public AttackController attackController;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player") && col.TryGetComponent<IDamageable>(out var _damageable))
        {
            _damageable.ApplyDamage(this.gameObject, attackController.currentWeapon.Damage);
        }
    }
}
