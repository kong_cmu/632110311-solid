using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour, IDamageable
{
    public static event Action<WeaponData> _OnChangedWeapon;

    private const int MaxHp = 5;
    public UnityEvent<int> onHpChanged;

    public static Action<WeaponData> OnChangedWeapon
    {
        get
        {
            return _OnChangedWeapon;
        }
        set
        {
            _OnChangedWeapon = value;
        }
    }

    public UnityEvent<int> _onHpChanged;

    [Header("Health")]
    [SerializeField] private int currentHp = MaxHp;

    [SerializeField] private Transform spawnPoint;

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }

    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }

    public void DecreaseHp(int _value)
    {
                                                                                                                                                                      
    }


    private void Respawn()
    {
        //rb.velocity = Vector2.zero;
        this.GetComponent<PlayerController>().ResetVelocity();
        transform.position = spawnPoint.position;
    }

    #endregion
    public void ApplyDamage(GameObject _source, int _damage)
    {
        currentHp -= _damage;

        if (currentHp <= 0)
        {
            Death(_source);
        }
        onHpChanged?.Invoke(currentHp);
    }
    private void Death(GameObject _source)
    {
        currentHp = MaxHp;
        Respawn();
    }
}
